#+NAME: Homepage
#+AUTHOR: Angelica Blevins
#+CO-AUTHOR: Zach Mandeville
#+TODO: DREAM MANIFESTING | REALITY

* Introduction  
  solarpunk.cool is an entry point into a world we want to help manifest, and a good landing point for various non-personal projects we are personally invested in.
* File Structure
  Let's have this be super simple and v. little javascript (except for things like detecting whether they are on dat or not.)  We want the entry point to be as accessible as possible.
#+NAME: Create Files
#+BEGIN_SRC shell :results value raw replace drawer
  touch index.html
  mkdir aesthetic
  touch aesthetic/main.css
  touch aesthetic/colors-and-fonts.css
  mkdir dat-zine-workshop
  touch dat-zine-workshop/index.html
  tree -I '.git'
#+END_SRC
#+RESULTS: Create Files
:RESULTS:
.
├── README.md
├── aesthetic
│   ├── colors-and-fonts.css
│   └── main.css
├── dat-zine-workshop
│   └── index.html
├── homepage.org
└── index.html

2 directories, 6 files
:END:
* Landing Page (index.html)
  :PROPERTIES:
  :header-args: :dir ./
  :END:
** The Code
   #+NAME: index.html
   #+BEGIN_SRC html :tangle ./index.html
     <!doctype HTML>
     <!-- 
          Hey!  Thanks for visiting this source code!  You are now 'one of the cool ones'.
          I'll give you a tour of sorts with these comments if you keen.

          Also, you want some good Wellington Music?  I really like St!
          https://girlboss.bandcamp.com/

          K, let's get to that code.
       -->
     <html>
       <head>
         <title>Solarpunk.cool</title>
         <!-- 
              Here's some meta deets to have the page be responsive, and have all my text display correctly.
              Without the charset meta stuff, quotation marks show up as &quot; which is considered &quot;Not Cool&quot;
           -->
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
         <meta name="viewport" content = "width=device=width, initial-scale=1">
         <!-- 
              Here's some meta deets for search engines and when the solarpunk link is shared with others.
           -->
         <meta name='author' content='yr friendly local solarpunks!'>
         <!-- Same, but now the facebook stuff -->
         <meta property="og:title" content="Solarpunk.Cool" />
         <meta property="og:type" content="website" />
         <meta name='description' content='The premier site for hot young solarpunks.'>
         <meta property="og:image"
               content="./assets/dat-zine-workshop-banner.png"
               />
         <meta property="og:url" content="https://coolguy.website/" />

         <!-- Now the Twitter stuff -->
         <meta name="twitter:card" content="summary_large_image" />
         <meta name="twitter:title" content="Solarpunk.Cool" />
         <meta name='twitter:description' content='The premier site for hot young solarpunks. />
         <meta name="twitter:image"
               content="./assets/dat-zine-workshop-banner.png"
               />
         <!-- Meta is done, now we can add that sweet styling! -->
         <link rel="stylesheet" type="text/css" href="styles/main.css" />
         <link rel='stylesheet' type='text/css' href='styles/colors-and-fonts.css' />
         <!-- We will also bring in the tachyons css because it's fun to code with. -->
         <link rel='stylesheet' type="text/css" href="https://unpkg.com/tachyons@4.10.0/css/tachyons.min.css" />
       </head>
     <!doctype HTML>
     <html>
     <body>
       <h1>solarpunk.cool</h1>
       <em>Build a better future through love, code, and hypnogogia. Achieve photosynthesis. Have fun.</em>
       <a href='dat-zine-workshop/' title='dat zine workshop event page'>Dat Zine Workshop</a>
     </body>
     </html>
   #+END_SRC
  
* Dat-Zine-Workshop
** Intro and Intentions
** index.html
*** Intentions
**** REALITY [8/8] The index page holds all the information for our dat zine workshop
     CLOSED: [2018-11-03 Sat 14:51]
     - [X] Banner: Make a Dat Zine! (using our nice pretty image)
     - [X] Header: An Introduction to Dat Zine-Making
     - [X] Subline: using punk2punk technology to foster creativity and community through code!
     - [X] Location: Thistle Hall
     - [X] Date: Sunday, Nov. 11th,
     - [X] Time: 11am to 12pm
     - [X] Description: Learn to make personal and unique digital zines using HTML, CSS, and Dat instead of paper, glue and staples.  You will be givent he tools and resources to make, discover, and share dat zines, and even create your own decentralized zine library.  No technical expreinece necessary, just bring your computer!
         (_let us know if you don't have access to a computer._)
     - [X] A Prelude to the Wellington Zine Fest
    
**** MANIFESTING You can click a link on the page and have it downloaded as an .ics file.
     We can do this using the hcalendar microformat for ical files.  There was a change in the class names for hcalendar, but we going to be over-careful and include the older and current classes just cos.  This is how [[https://shiftctrl.space][shift-ctrl space]] does it.

     Essentially, you add a class name that corresponds to a property within the ICS format.  The major parts of a calendar entry would be:

- vevent :: indicates that everything within is the hcalendar event or ical object or whatev's you wanna call it. Corresponds to the classes ~h-event vevent~.  We'll add these classes to a div that all the following classes are held within.     
- summary :: the name of the event, corresponds to the classes ~summary p-name~.
- url :: url for more details, it'll just be this url.  corresponds to ~u-url url~.
- dtstart :: the start time of the event.  ~dt-start dtstart~.
- dtend :: endtime natch. ~dt-end dtend~
- location :: where it is held. ~p-location location~
- description :: multi-paragraph description of event, can be written in html with the included links and all that. ~p-description description~
- attach :: any file attachment (often images).  we'll put our nice image into this. ~attach~

There is a site called [[https://htvx.com/ics][htvx.com]] that will parse web pages and turn them into ical events.  So  we can have a button that, when clicked, parses this whole page.  that way someone can download our webpage into their calendar?!?!  If we didn't want to rely on any other webpage, we can do the same thing with internal javascript.  This site has been up since 2004 tho, and is reliable.
**** DREAM The whole page is chunky and aesthetically pleasant.
**** DREAM The whole page looks good on a phone.
**** REALITY The Page is styled with tachyons too...?
     CLOSED: [2018-11-03 Sat 16:27]
**** DREAM The Page leads to a dat zine that is slightly different from the https page.
*** The Code     
 #+NAME: dat-zine-workshop/index.html
 #+BEGIN_SRC html :tangle ./dat-zine-workshop/index.html
   <!doctype HTML>
   <!-- 
        Hey!  Thanks for visiting this source code!  You are now 'one of the cool ones'.
        I'll give you a tour of sorts with these comments if you keen.

        Also, you want some good Wellington Music?  I really like Girlboss!
        https://girlboss.bandcamp.com/

        K, let's get to that code.
     -->
   <html>
     <head>
       <title>Dat Zine Making Workshop</title>
       <!-- 
            Here's some meta deets to have the page be responsive, and have all my text display correctly.
            Without the charset meta stuff, quotation marks show up as &quot; which is considered &quot;Not Cool&quot;
         -->
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
       <meta name="viewport" content = "width=device=width, initial-scale=1">
       <!-- 
            Here's some meta deets for search engines and when the solarpunk link is shared with others.
         -->
       <meta name='author' content='yr friendly local solarpunks!'>
       <!-- Same, but now the facebook stuff -->
       <meta property="og:title" content="Dat Zine Making Workshop | November 11th Thistle Hall, WLG" />
       <meta property="og:type" content="website" />
       <meta name='description' content='Dat Zine Workshop, November 11th, 2018 at Thistle Hall Wellington.'>
       <meta property="og:image" content="./assets/dat-zine-workshop-banner.png" />
       <meta property="og:url" content="https://coolguy.website/" />

       <!-- Now the Twitter stuff -->
       <meta name="twitter:card" content="summary_large_image" />
       <meta name="twitter:title" content="Dat Zine Making Workshop | November 11th Thistle Hall, WLG" />
       <meta name='twitter:description' content="Dat Zine Workshop, November 11th, 2018 at Thistle Hall Wellington." />
       <meta name="twitter:image" content="./assets/dat-zine-workshop-banner.png" />
       <!-- Meta is done, now we can add that sweet styling! -->
       <link rel="stylesheet" type="text/css" href="styles/main.css" />
       <link rel='stylesheet' type='text/css' href='styles/colors-and-fonts.css' />
       <!-- We will also bring in the tachyons css because it's fun to code with. -->
       <link rel='stylesheet' type="text/css" href="https://unpkg.com/tachyons@4.10.0/css/tachyons.min.css" />
     </head>
     <body class='bg-washed-yellow navy avenir'>
       <img src='./assets/dat-zine-workshop-banner.png' alt='two happy computers sharing zines with one another with dat zine workshop floating as a button above them. A computer hand is about to click the button.' /> 
       <main class='w-80 center flex flex-column items-center justify-center pb4 mb5'>
      <div class="h-event vevent" >
           <h1 class='tc summary p-name p-summary'>An Introduction to Dat Zine Making!</h1>
           <p class='tc'> using punk2punk technology to foster creativity and community through code! </p>
           <h3>Sunday, November 11th!</h3>
           <h3>Location</h3>
           <p>Thistle Hall</p>
           <h3>Time</h3>
           <p><abbr class="dt-start dtstart" title="2018-11-11T11:00:00+12">11am</abbr>
             to
           <abbr class="dt-end dtend" title="2018-11-11T13:30:00+12">1:30pm</abbr>
           </p>
           <h3>Tell us More!</h3>
           <p class="p-description description">Learn to make personal and unique digital zines using HTML, CSS, and Dat instead of paper, glue and staples.  You will be givent he tools and resources to make, discover, and share dat zines, and even create your own decentralized zine library.  No technical expreinece necessary, just bring your computer!</p>
           <em>And after you've done this workshop, check out our table(and all the other amazing things) at the Wellington Zine Fest, November 17th!</em>
        </div>
       </main>
         <div id='download-prompt' class="w-100 flex justify-center items-center fixed bottom-0 bg-washed-red pa2">
           <a class="f6 grow no-underline br-pill ph3 pv2 mb2 dib white bg-hot-pink" href="http://h2vx.com/ics/solarpunk.cool/dat-zine-workshop/index.html" title="downloads an .ics file that can be opened and saved in any common calendar app, phone or desktop or otherwise.">Add this Event to yr Calendar!</a>
           </div>
     </body>
   </html>
#+END_SRC

    
    

  
  

